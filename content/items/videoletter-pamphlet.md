---
title: Videoletter Pamphlet
itemType: gallery
poster: /archive/media/videoletter-pamphlet.png
keywords:
  - value: New York
  - value: Family
  - value: 1980s
date: 10.8.18
related: []
images:
  - caption:
    image: /archive/media/videoletter-pamphlet.png
---
A revolutionary project, VIDEOLETTERS brought 27 women's video groups from ultimately 17 communities together to broadcast news, stories and struggles of feminism, 1975-77. Today, completely unknown, this 2-page summary of the project aims to bring this model video exchange back into view for women's studies and media studies scholars, feminist community activists and others concerned with the history of feminism, media activism and global solidarity.
Copyright: © All Rights Reserved
Download as PDF, TXT or read online from Scribd

https://www.scribd.com/document/228461838/International-VIDEOLETTERS-Learn-View-Document-Study
