---
title: Bebashi
itemType: video
poster: /archive/media/bebashi_amfar_000001.jpg
collections: []
keywords:
  - value: AIDS
  - value: Activism
related: []
annotations:
  - body: >-
      Hendrerit urna a habitasse euismod dolor nec sagittis nunc fusce varius
      urna ullamcorper dui molestie scelerisque.
    end: '0:00:50'
    files:
      - file: ''
        filethumb: ''
    images:
      - image: /archive/media/__ia_thumb.jpg
      - image: >-
          /archive/media/mv5bnzuymgnmoditnmjhys00yzdlltlmmgutngqymtkyytjhntmyxkeyxkfqcgdeqxvynzi4mdmymtu-._v1_.jpg
    start: '0:00:30'
  - body: >-
      Nec nisl malesuada ad phasellus a turpis mus et adipiscing id nec
      consectetur gravida ac montes eu a laoreet pulvinar consectetur iaculis mi
      inceptos.
    end: '0:02:00'
    files:
      - file: ''
        filethumb: ''
    start: '0:01:45'
mediaUrl: 'https://archive.org/download/bebashi_amfar/bebashi_amfar.mp4'
---
Despite its low production values, cheap looking sets, and amateur presentation, the surface of the Bebashi tape's collection of short scenes belies a powerful representation of the sexual health issues surrounding African Americans and their communities of the period. Three different stories are presented (one in two parts) and share several actors. Addressed in these short vignettes are a myriad of difficult issues including — HIV/AIDS, domestic and child abuse, rape, drug use and rehabilitation, prostitution, gender norms, generational communication, and dying. A low-budget effort to explore these issues in a relatively brief and narrow frame seems like a recipe for almost certain failure but the boldness with which these controversial subjects are confronted -  including the raw authenticity of the dialogue, the complex nature of the circumstances presented, and the deeply-felt performances by the actors - combine to form something that is truly greater than the sum of its humble parts.

— *Adrian D. Cameron*
